# Clase #2 - Iniciando un webpack.config

Crearmos el archivo de configuración de webpack para facilitar su uso en mi caso use

`touch webpack.config.js`

Vamos agregando la configuración inicial al archivo

Este archivo permite importar módulos usando el formato commonJS y recibe por lo menos dos configuraciones básicas, un **entry** y un **output**.

- **Entry Point:** Es la ruta del archivo principal de nuestra aplicación JS a ser procesado por Webpack. Se pueden tener varios Entry Points.

- **Output:** Es la ruta de salida donde va a generar nuestro bundle con todos nuestros archivos especificados como Entry Points empaquetados en uno sólo.

- **mode**: Es el modo en el que sera compilado el código.

Ejemplo de esta configuración

```js
const path = require("path");

module.exports = {
  entry: "./index.js",
  mode: "development",
  output: {
    path: path.resolve(__dirname),
    filename: "bundle.js",
  },
};
```

Agregamos el comando en el `package.json` el comando para que todos los desarrolladores entiendan como se hace el build
