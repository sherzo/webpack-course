# Clase #4 - Multiples puntos de entrada

Aveces no tenemos un solo archivo desde el cual queremos cargar todas nuestra app
pueden tener varios puntos de entrada que corresponden muchas veces a diferentes páginas.

En esta clase creamos la carpeta `multy-entry-points` y dentro de ella `src` siguiendo las convenciones de donde se coloca el código fuente de un proyecto. Dentro de ella creamos la carpeta `js`.

Para crear multiples entry points lo que hacer es que el atributo entry lo cambiamos a un objecto
el cual tendra mediante llave valor el nombre y el archivo de cada entrada. Ejemplo:

```js
module.exports = {
  entry: {
    home: path.resolve(__dirname, "src/index.js"),
    prices: path.resolve(__dirname, "src/prices.js"),
    contact: path.resolve(__dirname, "src/contact.js"),
  },
  ...
};
// Tres archivos entrada
```

Ahora necesitamos modificar la salida de estos archivos para ello usamos un algo que nos provee webpack que son los templates. En este caso usaremos `name` lo cual no provee la nombre de la llave de cada una de las entradas

```js
module.exports = {
  entry: {
    home: path.resolve(__dirname, "src/index.js"),
    prices: path.resolve(__dirname, "src/prices.js"),
    contact: path.resolve(__dirname, "src/contact.js"),
  },
  mode: "development",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "js/[name].js", // Name corresponde la cada llave de cada entry.
  },
};
```