# Clase #1 - Primer bundle con webpack

Iniciamos un proyecto en javascript para ejemplificar el uso de webpack

`npm init`

Instalamos webpack

`npm i -D webpack`

Desde la versión 4 es necesario instalar webpack-cli

`npm i -D webpack-cli`

Podemos probar webpack usando `npx` de la siguiente manera

`npx webpack --entry ./index.js --output ./bundle.js`

Desde webpack 4 es necesario colocar el modo en el que esta webpack compilando nuestro codigo para usamos la flag `--mode development` para un tener un código de desarrolo

`npx webpack --entry ./index.js --output ./bundle.js --mode development`
