# Clase #5 - Manejo de assets con Loaders

**Los Loaders** son la funcionalidad que nos da Webpack para interpretar tipos de archivos no soportados de forma nativa por Javascript.

**style-loader** sirve para inyectar un tag style (el CSS) al DOM de nuestro HTML, mientras que css-loader sólo sirve para interpretar archivos CSS.

Para esta clase creamos la carpeta `css-style-loader` detro de ella haremos la configuración. Agragamos la propiedad `module` que tiene dentro la propiedad `rules` donde tiene las reglas. Para configurar el loader necesitamos la propiedad `test` recive una expresión regular que identifca cualquier archivo con extencion `.css` y con la propiedad `use` le indicamos webpack que loader va a utilizar.

```js
module.exports = {
  ...
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
```

Esto nos permitira importar un archiv `css` en nuestro código `js`

Pero antes debemos instalar los loaders que hacen posible que el interpretar `css-loader` el css y inyectarlo en nuestro html `style-loader`.

`npm i -D css-loader style-loader`
