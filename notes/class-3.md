# Clase #3 - Cargando configuración por defecto y personalizados

Por medio del uso de la bandera --config podemos especificar un archivo de configuración externo con el nombre que queramos en lugar del nombre por defecto webpack.config.js.

En esta clase creamos la capeta `external` y agregamos un nuevo archivo `webpack.config.js` dentro de ella junto con el codigo de nuestra app.

Para leer archivos externos de **webpack** usamos la flag `--config`

Agregamos el siguiente comando en el `package.json`

`"build:external": "webpack --config ./external/webpack.config.js"`
